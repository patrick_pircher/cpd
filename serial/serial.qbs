import qbs 1.0

Application {

    name: 'serial'

    Group {
        name: "Sources"
        files: [
            "*.c"
        ]
        Depends { name:"cpp" }
    }

    Group {
        name: "Headers"
        files: [
            "*.h"
        ]
    }
    Depends { name:"cpp" }
    cpp.cFlags: [
        "-fopenmp"
    ]
    cpp.linkerFlags: [
        "-static",
        "-fopenmp"
    ]

}


