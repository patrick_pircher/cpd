#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "wolves-squirrels-serial.h"
void sword__print();
#define MAX(a,b) a>b?a:b

World sworld;


void sworld__load(WorldParameters params, char *filename)
{
    sworld.params = params;
    FILE* pFile = fopen(filename,"r");
    char buffer[20];
    if (pFile == NULL){
        perror("Error opening file");
        exit(-1);
    }
    else
    {
        int first = 1;
        while ( !feof(pFile) )
        {
            if ( fgets(buffer , 20 , pFile) == NULL ) break;

            if( first ){
                int i;
                first = 0;
                sscanf(buffer,"%d",&sworld.size);

                //main world
                Cell** lines = (Cell**) calloc(sworld.size,sizeof(Cell*));
                for (i = 0; i < sworld.size; i++){
                    lines[i] = (Cell*) calloc(sworld.size,sizeof(Cell));
                }
                sworld.mainGrid = lines;

                //copy
                lines = (Cell**) calloc(sworld.size,sizeof(Cell*));
                for (i = 0; i < sworld.size; i++){
                    lines[i] = (Cell*) calloc(sworld.size,sizeof(Cell));
                }
                sworld.tmpGrid = lines;

            }else{

                int x,y;
                char type;
                sscanf(buffer,"%d %d %c",&x,&y,&type);

                Cell* cell = &sworld.mainGrid[y][x];
                cell->type = type;
                switch(cell->type)
                {
                case Wolf:
                    cell->breedingPeriod = sworld.params.wolvesBreedingPeriod;
                    cell->starvationPeriod = sworld.params.wolvesStarvationPeriod;
                    break;
                case TreeSquirrel:
                case Squirrel:
                    cell->breedingPeriod = sworld.params.squirrelsBreedingPeriod;
                    break;
                default:
                    break;
                }
                sworld.tmpGrid[y][x] = *cell;
            }
        }
        fclose (pFile);
        sword__print();
    }
}

/*! \internal
 * \brief sworld__copy
 *  copies the tmpGrid to mainGrid
 */
void sworld__copy()
{
    int x,y;
    for(x=0; x<sworld.size; x++)
    {
        for(y=0; y<sworld.size; y++)
        {
            sworld.mainGrid[x][y] = sworld.tmpGrid[x][y];
        }
    }
}

/*! \internal
 *\brief sworld__next_cell determines the next cell to move to
 *  This function applies "Rules for Selecting a Cell when Multiple Choices Are Possible"
 *\param p - current cell with Wolv or Squirrel
 *\return a cell to which the Wolv or Squirrel can move to. The cell is in the tmpGrid
 */
Cell* sworld__next_cell(Point p, int poss, int dir[4])
{
    int choice, C;
    Cell* selected;
    int choices[4] = {0};
    int i;
    int j=0;

    enum{Up,Right,Down,Left};
    //create list of valid directions
    //enumerate directions: up=0;right=1;...
    for(i=0; i<4;i++){
        if(dir[i]==1){
            choices[j] = i;
            j++;
        }
    }

    C = p.y * sworld.size + p.x;
    choice = C % poss;

    switch(choices[choice]){
    case Up:
        selected = &sworld.tmpGrid[p.x][p.y-1];
        break;
    case Right:
        selected = &sworld.tmpGrid[p.x+1][p.y];
        break;
    case Down:
        selected = &sworld.tmpGrid[p.x][p.y+1];
        break;
    case Left:
        selected = &sworld.tmpGrid[p.x-1][p.y];
        break;
    }
    return selected;
}

/*!
 * \brief sworld__wolf_process_move
 *  moves wolf and checks for conflicts
 * \param squirrel squirrel in mainGrid
 * \param moveTo position in tmpGrid
 */
void sworld__wolf_process_move(Cell* wolf, Cell* moveTo)
{
    Cell tmp = {0};

    /* When a wolf breeds the breeding period is reseted
     * and then the wolf moves and a new wolf remains at the old
     * position
     */
    if(wolf->breedingPeriod == 0){
        tmp.type = Wolf;
        tmp.starvationPeriod = sworld.params.wolvesStarvationPeriod;
        tmp.breedingPeriod = sworld.params.wolvesBreedingPeriod;

        wolf->breedingPeriod = sworld.params.wolvesBreedingPeriod;
    }else{
        tmp.type = Empty;
    }

    //solve conlficts or just move
    switch(moveTo->type){

    case Squirrel:
        moveTo->starvationPeriod = sworld.params.wolvesStarvationPeriod;
        moveTo->type = Wolf;
        moveTo->breedingPeriod = wolf->breedingPeriod;
        break;

    case Wolf:
        if( wolf->starvationPeriod == moveTo->starvationPeriod ){

            moveTo->breedingPeriod = MAX(moveTo->breedingPeriod,wolf->breedingPeriod);
        }else{

            if( wolf->starvationPeriod > moveTo->starvationPeriod ){
                moveTo->starvationPeriod = wolf->starvationPeriod;
                moveTo->breedingPeriod = wolf->breedingPeriod;
            }
        }
        break;
    case Empty:

        moveTo->type = Wolf;
        moveTo->starvationPeriod = wolf->starvationPeriod;
        moveTo->breedingPeriod = wolf->breedingPeriod;
        break;
    default:
        break;
    }

    //old cell empty or with new wolf
    *wolf = tmp;
}


/*! \internal
 * \brief sworld__check_wolf applies "Rules for Wolves"
 *  also solves conflicts
 * \param p
 */
void sworld__check_wolf(Point p)
{
    int N, possSq=0, possEmpty=0;
    int dirSq[4] = {0};
    int dirEmpty[4] = {0};
    Cell *current, *up=NULL, *right=NULL, *down=NULL, *left=NULL, *selected=NULL;

    current = &sworld.mainGrid[p.x][p.y];
    N = sworld.size;

    //the cell of mainGrid must not be changed
    Cell tmp = *current;
    if(current->starvationPeriod == 0){
        tmp.type = Empty;
        sworld.tmpGrid[p.x][p.y] = tmp;
        return;
        /*Wolf dies*/
    }

    /*Checks for possible cells to move to*/
    /*Up*/
    if(p.y > 0){
        up = &sworld.mainGrid[p.x][p.y-1];
        if( up->type == Empty ){
            dirEmpty[0] = 1;
            possEmpty++;
        }else if( up->type == Squirrel ){
            dirSq[0] = 1;
            possSq++;
        }
    }
    /*Right*/
    if(p.x < N-1){
        right = &sworld.mainGrid[p.x+1][p.y];
        if( right->type == Empty ){
            dirEmpty[1] = 1;
            possEmpty++;
        }else if( right->type == Squirrel ){
            dirSq[1] = 1;
            possSq++;
        }
    }
    /*Down*/
    if(p.y < N-1){
        down = &sworld.mainGrid[p.x][p.y+1];
        if( down->type == Empty ){
            dirEmpty[2] = 1;
            possEmpty++;
        }else if( down->type == Squirrel ){
            dirSq[2] = 1;
            possSq++;
        }
    }
    /*Left*/
    if(p.x > 0){
        left = &sworld.mainGrid[p.x-1][p.y];
        if( left->type == Empty ){
            dirEmpty[3] = 1;
            possEmpty++;
        }else if( left->type == Squirrel ){
            dirSq[3] = 1;
            possSq++;
        }
    }

    if(possSq == 0 && possEmpty == 0){
        /*Stays in the same place*/
        return;
    }else {
        if(possSq > 0){
            selected = sworld__next_cell(p, possSq, dirSq);
        }else{
            selected = sworld__next_cell(p, possEmpty, dirEmpty);
        }
    }

    //process move: selected is already a reference to a cell in tmpGrid
    sworld__wolf_process_move(&tmp, selected);
    sworld.tmpGrid[p.x][p.y] = tmp;
    return;
}

/*!
 * \brief sworld__squirrel_process_move
 *  moves squirrel and checks for conflicts
 * \param squirrel squirrel in mainGrid
 * \param moveTo position in tmpGrid
 */
void sworld__squirrel_process_move(Cell* squirrel, Cell* moveTo)
{
    Cell tmp = {0};

    /* When a squirrel breeds the breeding period is reseted
     * and then the squirrel moves and a new squirrel remains at the old
     * position
     * new cell at old position will be tmp
     */
    if(squirrel->breedingPeriod == 0){
        tmp.type = squirrel->type;
        tmp.breedingPeriod = sworld.params.squirrelsBreedingPeriod;

        squirrel->breedingPeriod = sworld.params.squirrelsBreedingPeriod;
    }else{
        if(squirrel->type == TreeSquirrel){
            tmp.type = Tree;
        }else{
            tmp.type = Empty;
        }
    }

    switch(moveTo->type){
    case Empty:
        moveTo->type = Squirrel;
        moveTo->breedingPeriod = squirrel->breedingPeriod;
        break;
    case Wolf:
        moveTo->starvationPeriod = sworld.params.wolvesStarvationPeriod;
        break;
    case Tree:
        moveTo->type = TreeSquirrel;
        moveTo->breedingPeriod = squirrel->breedingPeriod;
        break;
    case Squirrel:
    case TreeSquirrel:
            moveTo->breedingPeriod = MAX(moveTo->breedingPeriod,squirrel->breedingPeriod);
        break;
    case Ice:
        break;
    }

    //old cell empty or with new squirrel
    *squirrel = tmp;
}

/*! \internal
 * \brief sworld__check_squirrel applies "Rules for Squirrels"
 *  also solves conflicts
 * \param p
 */
void sworld__check_squirrel(Point p)
{
    int poss, N, dir[4]={0};
    Cell *current, *up, *right, *down, *left, *selected;

    current = &sworld.mainGrid[p.x][p.y];
    N = sworld.size;


    /*Checks for possible cells to move to*/
    poss = 0;
    /*Up*/
    if(p.y > 0){
        up = &sworld.mainGrid[p.x][p.y-1];
        if( up->type == Tree || up->type == Empty){
            dir[0] = 1;
            poss++;
        }
    }
    /*Right*/
    if(p.x < N-1){
        right = &sworld.mainGrid[p.x+1][p.y];
        if( right->type == Tree || right->type == Empty){
            dir[1] = 1;
            poss++;
        }
    }
    /*Down*/
    if(p.y < N-1){
        down =  &sworld.mainGrid[p.x][p.y+1];
        if(down->type == Tree || down->type == Empty){
            dir[2] = 1;
            poss++;
        }
    }
    /*Left*/
    if(p.x > 0){
        left = &sworld.mainGrid[p.x-1][p.y];
        if( left->type == Tree || left->type == Empty){
            dir[3] = 1;
            poss++;
        }
    }

    /*Moves*/
    if(poss == 0){
        /*Stays in the same place*/
        sworld.tmpGrid[p.x][p.y] = *current;
        return;
    }else{
        selected = sworld__next_cell(p, poss, dir);
    }

    //the cell of mainGrid must not be changed
    Cell tmp = *current;
    sworld__squirrel_process_move(&tmp, selected);
    sworld.tmpGrid[p.x][p.y] = tmp;
    return;
}
/*!
 * \brief sworld__process_cell process cells Wolf and Squirrels
 * \param p position of cell in grid
 */
void sworld__process_cell(Point p)
{
    Cell* cell = &sworld.mainGrid[p.x][p.y];
    switch(cell->type){
    case(Wolf):
        sworld__check_wolf(p);
        break;
    case(TreeSquirrel):
    case(Squirrel):
        sworld__check_squirrel(p);
        break;
    default:
        break;
    }
}

void sworld__reduce_period(Point p)
{
    Cell* cell = &sworld.mainGrid[p.x][p.y];
    switch(cell->type){
    case(Wolf):
        cell->starvationPeriod--;
    case(TreeSquirrel):
    case(Squirrel):
        if(cell->breedingPeriod>0){
            cell->breedingPeriod--;
        }
    default:
        break;
    }
    //also applies to tmp grid
    sworld.tmpGrid[p.x][p.y] = *cell;
}

/*!
 * \brief world_run runs the simulation
 *  The simulations is done using the red-black scheme
 */
void sworld__run()
{
    int i;
    for(i=0; i<sworld.params.iterations; i++){

        int j,k;

        //first red cells, then black cells
        //red cells -> process even lines with even columns
        //             and odd lines with odd columns
        for(j=0; j<sworld.size; j++){
            for(k=j%2; k<sworld.size; k+=2){
                Point p = {j,k};
                sworld__process_cell(p);
            }
        }

        sworld__copy();
        sword__print();


        //black cells -> process even lines with odd columns
        //               and odd lines with even columns
        for(j=0; j<sworld.size; j++){
            for(k=(1-j%2); k<sworld.size; k+=2){
                Point p = {j,k};
                sworld__process_cell(p);
            }
        }
        sworld__copy();

        //reduce periods
        for(j=0; j<sworld.size; j++){
            for(k=0; k<sworld.size; k++){
                Point p = {j,k};
                sworld__reduce_period(p);
            }
        }
        sword__print();

    }
}


/*!
 * \brief sworld__save saves status of the world to \a filename or prints to console
 * \param filename filename to write to when  toConsole is 0
 * \param toConsole print to stdout when 1
 */
void sworld__save(char* filename, int toConsole)
{
    int y,x;
    FILE* file;
    if(toConsole){
        file = stdout;
    }else{
        file = fopen(filename,"w");
    }

    fprintf(file,"%d\n", sworld.size);
    for(x=0; x<sworld.size; x++){

        for(y=0; y<sworld.size; y++){

            if(sworld.mainGrid[y][x].type){
                fprintf(file,"%d %d %c\n",x,y, sworld.mainGrid[y][x].type);
            }
        }
    }
    if(!toConsole){
        fclose(file);
    }
}




void sworld__destroy()
{
    int i;
    for(i=0; i<sworld.size; i++){

        free(sworld.mainGrid[i]);
        free(sworld.tmpGrid[i]);
    }
    free(sworld.mainGrid);
    free(sworld.tmpGrid);
}



void sword__print()
{
    return;
    int y,x;
    printf("  | %2d | ",0);
    for(y=1; y<sworld.size; y++){
        printf("%2d | ",y);
    }
    printf("\n");

    for(y=0; y<sworld.size; y++){
        printf("%2d|",y);
        for(x=0; x<sworld.size; x++){

            if(sworld.mainGrid[x][y].type){
                printf("  %c  ", sworld.mainGrid[x][y].type);
            }else{
                printf("  -  ");
            }
        }
        printf("\n");
    }
    printf("\n");
}
