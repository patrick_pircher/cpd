#ifndef WOLVESSQUIRRELSERIAL_H
#define WOLVESSQUIRRELSERIAL_H


typedef struct WorldParameters{
    int wolvesBreedingPeriod;
    int squirrelsBreedingPeriod;
    int wolvesStarvationPeriod;
    int iterations;
}WorldParameters;

typedef enum Entity{
    Wolf = 'w',
    Squirrel = 's',
    Tree = 't',
    Ice = 'i',
    TreeSquirrel = '$',
    Empty = 0
}Entity;

typedef struct Cell {
    Entity type;
    int breedingPeriod;
    int starvationPeriod;
} Cell, **Grid;

typedef struct World{
    WorldParameters params;
    int size;
    Grid mainGrid;
    Grid tmpGrid;
}World;

typedef struct Point {
    int x,y;
}Point;


void sworld__load(WorldParameters params, char *filename);
void sworld__run();
void sworld__save(char* filename, int toConsole);
void sworld__destroy();



#endif // WOLVESSQUIRRELSERIAL_H
