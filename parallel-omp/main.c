#include "wolves-squirrels-parallel.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char** argv)
{
    int nThreads = 0;
    int toConsole = 0;
    WorldParameters params;
    params.iterations = 0;
    params.squirrelsBreedingPeriod = 0;
    params.wolvesBreedingPeriod = 0;
    params.wolvesStarvationPeriod = 0;


    if(argc<6){
        printf("not enough arguments\n");
        return -1;
    }

    if(!(sscanf(argv[2], "%d", &params.wolvesBreedingPeriod)
            && sscanf(argv[3], "%d", &params.squirrelsBreedingPeriod)
            && sscanf(argv[4], "%d", &params.wolvesStarvationPeriod)
            && sscanf(argv[5], "%d", &params.iterations))){
        printf("arguments not valid\n");
        return -1;
    }

    if(argc==7){
        sscanf(argv[6], "%d", &nThreads);
    }
    if(argc==8){
        sscanf(argv[7], "%d", &toConsole);
    }

    if(nThreads){
        omp_set_num_threads(nThreads);
    }

    char* filename = argv[1];
    sworld__load(params,filename);
    double start = omp_get_wtime();
    sworld__run();

    int index = strlen(filename)-2;
    char filename_out[256];
    strcpy(filename_out,filename);

    filename_out[index] = '\0';

    strcat(filename_out, "out");


    double end = omp_get_wtime();

    sworld__save(filename_out, toConsole);
    sworld__destroy();

    fprintf(stderr,"time: %f\n", end-start);
    return 1;
}
