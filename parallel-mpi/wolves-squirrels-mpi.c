#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <string.h>


#include "wolves-squirrels-mpi.h"

#define MAX(a,b) a>b?a:b

enum{Up,Right,Down,Left};


void world__print(World *world);
void world__copy_cell(Cell* cell1, Cell* cell2);
void world__wolf_process_move(World *world, Cell *wolf, Cell *moveTo);
void world__squirrel_process_move(World *world, Cell *squirrel, Cell *moveTo);

void world__allocate(World* world)
{
    int start = (world->mpi_start>0)?world->mpi_start-1:world->mpi_start;
    int end   = (world->mpi_end<world->size)?world->mpi_end+1:world->mpi_end;
    int i;

    Cell** lines = (Cell**) calloc(world->size,sizeof(Cell*));
    for (i = start; i < end; i++){
        lines[i] = (Cell*) calloc(world->size,sizeof(Cell));
    }
    world->mainGrid = lines;

    //copy
    lines = (Cell**) calloc(world->size,sizeof(Cell*));
    for (i = start; i < end; i++){
        lines[i] = (Cell*) calloc(world->size,sizeof(Cell));
    }
    world->tmpGrid = lines;
}

void world__set_data(World* world,int x, int y, char type)
{
    Cell* cell = &world->mainGrid[y][x];
    cell->type = type;
    switch(cell->type)
    {
    case Wolf:
        cell->breedingPeriod = world->params.wolvesBreedingPeriod;
        cell->starvationPeriod = world->params.wolvesStarvationPeriod;
        break;
    case TreeSquirrel:
    case Squirrel:
        cell->breedingPeriod = world->params.squirrelsBreedingPeriod;
        break;
    default:
        break;
    }

    world__copy_cell(cell, &world->tmpGrid[y][x]);
}

World* world__load(WorldParameters params, char *filename)
{
    int rank, nTotalProcs;
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
    MPI_Comm_size (MPI_COMM_WORLD, &nTotalProcs);	/* get number of processes */
	World* world = (World*) calloc(1,sizeof(World));
    world->params = params;
    world->mpi_rank = rank;

    if( rank == 0 ){

        FILE* pFile = fopen(filename,"r");
        char buffer[20];
        if (pFile == NULL){
            perror("Error opening file");
            exit(-1);
        }
        else
        {
            int first = 1;
            while ( !feof(pFile) )
            {
                if ( fgets(buffer , 20 , pFile) == NULL ) break;

                if( first ){

                    first = 0;
                    sscanf(buffer,"%d",&world->size);
                    int minLines = ceil(1.0 * world->size/nTotalProcs);
                    if(minLines==0){
                        minLines=1;
                    }
                    world->mpi_start = rank * minLines;
                    world->mpi_end = rank * minLines + minLines;
                    if(world->mpi_end > world->size){
                        world->mpi_end = world->size;
                    }
                    world__allocate(world);

                    MPI_Bcast(&world->size, 1, MPI_INT,0,MPI_COMM_WORLD);


                }else{

                    int x,y;
                    char type;
                    sscanf(buffer,"%d %d %c",&x,&y,&type);
                    int minLines = ceil(1.0 * world->size/nTotalProcs);
                    if(minLines==0){
                        minLines=1;
                    }

                    int start = (world->mpi_start>0)?world->mpi_start-1:world->mpi_start;
                    int end   = (world->mpi_end<world->size)?world->mpi_end+1:world->mpi_end;
                    int sendTo = y / minLines;
                    int sendToBefore = (y-1) / minLines;
                    int sendToAfter = (y+1) / minLines;

                    if(sendTo && sendTo<nTotalProcs){
                        int data[3] = {x,y,type};
                        MPI_Send(data,3,MPI_INT,sendTo,sendTo,MPI_COMM_WORLD);
                    }

                    if(sendToBefore>0 && sendToBefore != sendTo){
                        int data[3] = {x,y,type};
                        MPI_Send(data,3,MPI_INT,sendToBefore,sendToBefore,MPI_COMM_WORLD);
                    }

                    if(sendToAfter<world->size && sendToBefore != sendTo){
                        int data[3] = {x,y,type};
                        MPI_Send(data,3,MPI_INT,sendToAfter,sendToAfter,MPI_COMM_WORLD);
                    }

                    if(y<start || y>=end){
                        continue;
                    }
                    world__set_data(world,x,y,type);
                }
            }
            int i;
            int data[3] = {-1,-1,-1};
            for(i=1;i<nTotalProcs;i++){
                MPI_Send(data,3,MPI_INT,i,i,MPI_COMM_WORLD);
            }
            fclose (pFile);
        }
    }else{
        int data[3];
        MPI_Status status;

        MPI_Bcast(&world->size, 1, MPI_INT,0,MPI_COMM_WORLD);
        int minLines = ceil(1.0 * world->size/nTotalProcs);
        if(minLines==0){
            minLines=1;
        }
        world->mpi_start = rank * minLines;
        world->mpi_end = rank * minLines + minLines;
        if(world->mpi_end > world->size){
            world->mpi_end = world->size;
        }
        world__allocate(world);

        while(1){

            MPI_Recv(data,3,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
            if(data[0]==-1){
                break;
            }
            world__set_data(world,data[0],data[1],(char)data[2]);
        }
    }
    return world;
}

/*!
 * \brief world_copy_cell copies cell1 to cell2 except the lock variable
 * \param cell1
 * \param cell2
 */
void world__copy_cell(Cell* cell1, Cell* cell2)
{
	*cell2 = *cell1;
}


void world__merge_line(World* world, Cell* line, Cell* otherLine, int l){

	int i;
	for(i=0; i<world->size; i++){

		Cell* otherCell = &otherLine[i];
		Cell* cell = &line[i];


		if( otherCell->changed && cell->changed){
			if(otherCell->type==Wolf){
				world__wolf_process_move(world, otherCell, &line[i]);
			}
			if(otherCell->type==Squirrel){
				world__squirrel_process_move(world, otherCell, &line[i]);
			}

		}else if( otherCell->changed && !cell->changed){

			world__copy_cell(otherCell,cell);
		}

		cell->changed = 0;
	}
}

void world__recv(World* world, Cell* tmpLine, int fromRank){

	MPI_Status status;
	MPI_Recv(tmpLine,
			 world->size*sizeof(Cell)/sizeof(int),
			 MPI_INT,
			 fromRank,
			 MPI_ANY_TAG,
			 MPI_COMM_WORLD,
			 &status);
}

void world__send(World* world, Cell* tmpLine, int toRank){
	MPI_Send(tmpLine,
			 world->size*sizeof(Cell)/sizeof(int),
			 MPI_INT,
			 toRank,
			 toRank,
			 MPI_COMM_WORLD);
}

/**
 * @brief world__sync
 * @param world
 */
void world__sync(World* world){

    Cell tmpLine[world->size];
	if(world->mpi_start>0){

		int index = world->mpi_start;

		world__send(world, world->tmpGrid[index-1], world->mpi_rank-1);
        world__recv(world, tmpLine, world->mpi_rank-1);
        world__merge_line(world,world->tmpGrid[index-1], tmpLine, index-1);


		world__send(world, world->tmpGrid[index], world->mpi_rank-1);
		world__recv(world, tmpLine,world->mpi_rank-1);
		world__merge_line(world,world->tmpGrid[index], tmpLine, index);

	}

	if(world->mpi_end<world->size){

		int index = world->mpi_end-1;

        world__recv(world, tmpLine, world->mpi_rank+1);
		world__send(world, world->tmpGrid[index], world->mpi_rank+1);
        world__merge_line(world,world->tmpGrid[index], tmpLine, index);

        world__recv(world, tmpLine, world->mpi_rank+1);
		world__send(world, world->tmpGrid[index+1], world->mpi_rank+1);
		world__merge_line(world,world->tmpGrid[index+1], tmpLine, index+1);
	}
}

/*! \internal
 * \brief world__copy
 *  copies the tmpGrid to mainGrid
 */
void world__copy(World* world)
{
    int y;

#pragma omp for schedule(static) private(y)
	int start = (world->mpi_start>0)?world->mpi_start-1:world->mpi_start;
	int end   = (world->mpi_end<world->size)?world->mpi_end+1:world->mpi_end;
	for(y=start; y<end; y++)
	{
        memcpy(world->mainGrid[y],world->tmpGrid[y], world->size*sizeof(Cell));
	}
}

/*! \internal
 *\brief world__next_cell determines the next cell to move to
 *  This function applies "Rules for Selecting a Cell when Multiple Choices Are Possible"
 *\param p - current cell with Wolv or Squirrel
 *\return a cell to which the Wolv or Squirrel can move to. The cell is in the tmpGrid
 */

Cell* world__next_cell(World* world, Point p, int poss, int choices[4])
{
    int choice=0;

    if(poss>1){
        choice = (p.y * world->size + p.x) % poss;
    }

	switch(choices[choice]){
	case Up:
        return &world->tmpGrid[p.x][p.y-1];
	case Right:
        return &world->tmpGrid[p.x+1][p.y];
	case Down:
        return &world->tmpGrid[p.x][p.y+1];
	case Left:
        return &world->tmpGrid[p.x-1][p.y];
	}
}

/*!
 * \brief world__wolf_process_move
 *  moves wolf and checks for conflicts
 * \param squirrel squirrel in mainGrid
 * \param moveTo position in tmpGrid
 */
void world__wolf_process_move(World* world, Cell* wolf, Cell* moveTo)
{
	Cell tmp = {0};

	tmp.changed = 1;
	moveTo->changed = 1;

	/* When a wolf breeds the breeding period is reseted
	 * and then the wolf moves and a new wolf remains at the old
	 * position
	 */
	if(wolf->breedingPeriod == 0){
		tmp.type = Wolf;
		tmp.starvationPeriod = world->params.wolvesStarvationPeriod;
		tmp.breedingPeriod = world->params.wolvesBreedingPeriod;

		wolf->breedingPeriod = world->params.wolvesBreedingPeriod;
	}else{
		tmp.type = Empty;
	}

	//omp_set_lock(&moveTo->lock);
	//solve conlficts or just move
	switch(moveTo->type){

	case Squirrel:
		moveTo->starvationPeriod = world->params.wolvesStarvationPeriod;
		moveTo->type = Wolf;
		moveTo->breedingPeriod = wolf->breedingPeriod;
		break;

	case Wolf:
		if( wolf->starvationPeriod == moveTo->starvationPeriod ){

			moveTo->breedingPeriod = MAX(moveTo->breedingPeriod,wolf->breedingPeriod);
		}else{

			if( wolf->starvationPeriod > moveTo->starvationPeriod ){
				moveTo->starvationPeriod = wolf->starvationPeriod;
				moveTo->breedingPeriod = wolf->breedingPeriod;
			}
		}
		break;
	case Empty:

		moveTo->type = Wolf;
		moveTo->starvationPeriod = wolf->starvationPeriod;
		moveTo->breedingPeriod = wolf->breedingPeriod;
		break;
	default:
		break;
	}
	//omp_unset_lock(&moveTo->lock);

	//old cell empty or with new wolf
	*wolf = tmp;
}


/*! \internal
 * \brief world__check_wolve applies "Rules for Wolves"
 *  also solves conflicts
 * \param p
 */
void world__check_wolf(World* world, Point p)
{
	int N, possSq=0, possEmpty=0;
    int dirSq[4];
    int dirEmpty[4];
    Cell *current, *selected=NULL;

	current = &world->mainGrid[p.x][p.y];

	//the cell of mainGrid must not be changed
	Cell tmp = *current;
	if(current->starvationPeriod == 0){
		tmp.type = Empty;
		tmp.changed = 1;
		world->tmpGrid[p.x][p.y] = tmp;
		return;
		/*Wolf dies*/
	}

	/*Checks for possible cells to move to*/
	/*Up*/
	if(p.y > 0){
        selected = &world->mainGrid[p.x][p.y-1];
        if( selected->type == Empty ){
            dirEmpty[possEmpty++] = Up;
        }else if( selected->type == Squirrel ){
            dirSq[possSq++] = Up;
		}
	}
	/*Right*/
	if(p.x < world->size-1){
        selected = &world->mainGrid[p.x+1][p.y];
        if( selected->type == Empty ){
            dirEmpty[possEmpty++] = Right;
        }else if( selected->type == Squirrel ){
            dirSq[possSq++] = Right;
		}
	}
	/*Down*/
	if(p.y < world->size-1){
        selected = &world->mainGrid[p.x][p.y+1];
        if( selected->type == Empty ){
            dirEmpty[possEmpty++] = Down;
        }else if( selected->type == Squirrel ){
            dirSq[possSq++] = Down;
		}
	}
	/*Left*/
	if(p.x > 0){
        selected = &world->mainGrid[p.x-1][p.y];
        if( selected->type == Empty ){
            dirEmpty[possEmpty++] = Left;
        }else if( selected->type == Squirrel ){
            dirSq[possSq++] = Left;
		}
	}

	if(possSq == 0 && possEmpty == 0){
		/*Stays in the same place*/
		return;
	}else {
		if(possSq > 0){
			selected = world__next_cell(world,p, possSq, dirSq);
		}else{
			selected = world__next_cell(world,p, possEmpty, dirEmpty);
		}
	}

	//process move: selected is already a reference to a cell in tmpGrid
	world__wolf_process_move(world,&tmp, selected);
	world__copy_cell(&tmp,&world->tmpGrid[p.x][p.y]);
	return;
}

/*!
 * \brief world__squirrel_process_move
 *  moves squirrel and checks for conflicts
 * \param squirrel squirrel in mainGrid
 * \param moveTo position in tmpGrid
 */
void world__squirrel_process_move(World* world, Cell* squirrel, Cell* moveTo)
{
	Cell tmp = {0};
	tmp.changed = 1;
	moveTo->changed = 1;

	/* When a squirrel breeds the breeding period is reseted
	 * and then the squirrel moves and a new squirrel remains at the old
	 * position
	 * new cell at old position will be tmp
	 */
	if(squirrel->breedingPeriod == 0){
		tmp.type = squirrel->type;
		//tmp.lock = squirrel->lock;
		tmp.breedingPeriod = world->params.squirrelsBreedingPeriod;

		squirrel->breedingPeriod = world->params.squirrelsBreedingPeriod;
	}else{
		if(squirrel->type == TreeSquirrel){
			tmp.type = Tree;
		}else{
			tmp.type = Empty;
		}
	}

	//omp_set_lock(&moveTo->lock);
	switch(moveTo->type){
	case Empty:
		moveTo->type = Squirrel;
		moveTo->breedingPeriod = squirrel->breedingPeriod;
		break;
	case Wolf:
		moveTo->starvationPeriod = world->params.wolvesStarvationPeriod;
		break;
	case Tree:
		moveTo->type = TreeSquirrel;
		moveTo->breedingPeriod = squirrel->breedingPeriod;
		break;
	case Squirrel:
	case TreeSquirrel:
		moveTo->breedingPeriod = MAX(moveTo->breedingPeriod,squirrel->breedingPeriod);
		break;
	case Ice:
		break;
	}

	//omp_unset_lock(&moveTo->lock);
	//old cell empty or with new squirrel
	*squirrel = tmp;
}

/*! \internal
 * \brief world__check_squirrel applies "Rules for Squirrels"
 *  also solves conflicts
 * \param p
 */
void world__check_squirrel(World* world, Point p)
{
	int poss, N, dir[4]={0};
    Cell *current, *selected;

	current = &world->mainGrid[p.x][p.y];


	/*Checks for possible cells to move to*/
	poss = 0;
	/*Up*/
	if(p.y > 0){
        selected = &world->mainGrid[p.x][p.y-1];
        if( selected->type == Empty || selected->type == Tree){
            dir[poss++] = Up;
		}
	}
	/*Right*/
	if(p.x < world->size-1){
        selected = &world->mainGrid[p.x+1][p.y];
        if( selected->type == Empty || selected->type == Tree){
            dir[poss++] = Right;
		}
	}
	/*Down*/
	if(p.y < world->size-1){
        selected =  &world->mainGrid[p.x][p.y+1];
        if(selected->type == Empty || selected->type == Tree){
            dir[poss++] = Down;
		}
	}
	/*Left*/
	if(p.x > 0){
        selected = &world->mainGrid[p.x-1][p.y];
        if( selected->type == Empty || selected->type == Tree){
            dir[poss++] = Left;
		}
	}

	/*Moves*/
	if(poss == 0){
		/*Stays in the same place*/
		world->tmpGrid[p.x][p.y] = *current;
		return;
	}else{
		selected = world__next_cell(world,p, poss, dir);
	}

	//the cell of mainGrid must not be changed
	Cell tmp = *current;
	world__squirrel_process_move(world,&tmp, selected);
	world__copy_cell(&tmp,&world->tmpGrid[p.x][p.y]);
	return;
}
/*!
 * \brief world__process_cell process cells
 * \param p position of cell in grid
 */
void world__process_cell(World* world, Point p)
{
    if(world->tmpGrid[p.x][p.y].type==Empty) return;
	Cell* cell = &world->mainGrid[p.x][p.y];
	switch(cell->type){
	case(Wolf):
		world__check_wolf(world,p);
		break;
	case(TreeSquirrel):
	case(Squirrel):
		world__check_squirrel(world,p);
		break;
	default:
		break;
	}
}

void world__reduce_period(World* world, Point p)
{
    if(world->tmpGrid[p.x][p.y].type==Empty) return;

    Cell* cell = &world->tmpGrid[p.x][p.y];
	switch(cell->type){
	case(Wolf):
		cell->starvationPeriod--;
	case(TreeSquirrel):
	case(Squirrel):
		if(cell->breedingPeriod>0){
			cell->breedingPeriod--;
		}
	default:
		break;
	}
}

/*!
 * \brief world_run runs the simulation
 *  The simulations is done using the red-black scheme
 */
void world__run(World* world)
{
	int i=0;
    Point p;
	world__print(world);
#pragma omp parallel private(i)
	{
		for(i=0;i<world->params.iterations;i++){
			int j,k;

			//first red cells, then black cells
			//red cells -> process even lines with even columns
			//             and odd lines with odd columns
#pragma omp for schedule(static) private(k)

			for(j=world->mpi_start; j<world->mpi_end; j++){
                for(k=j%2; k<world->size; k+=2){
                    p.x = j;p.y=k;
                    world__process_cell(world,p);
				}
			}

			//printf("%d %d red\n\n", world->mpi_rank, i);

			world__sync(world);
			world__copy(world);


			//black cells -> process even lines with odd columns
			//               and odd lines with even columns
#pragma omp for schedule(static) private(k)
			for(j=world->mpi_start; j<world->mpi_end; j++){
				for(k=(1-j%2); k<world->size; k+=2){
                    p.x = j;p.y=k;
					world__process_cell(world,p);
				}
			}

			//printf("%d %d black\n\n", world->mpi_rank, i);

            world__sync(world);

			//reduce periods
#pragma omp for schedule(static) private(k)
			for(j=world->mpi_start; j<world->mpi_end; j++){
				for(k=0; k<world->size; k++){
                    p.x = j;p.y=k;
					world__reduce_period(world,p);
				}
			}
            world__copy(world);
		}
	}
}

/*!
 * \brief world__save saves status of the world to \a filename or prints to console
 * \param filename filename to write to when  toConsole is 0
 * \param toConsole print to stdout when 1
 */
void world__save(World* world)
{
	int y,x;
	int i,l, rank, nTotalProcs;
	Cell line[world->size];
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
	MPI_Comm_size (MPI_COMM_WORLD, &nTotalProcs);	/* get number of processes */
	MPI_Status status;
	int minLines = ceil(1.0 * world->size/nTotalProcs);
	if(minLines==0){
		minLines=1;
	}

	printf("%d\n", world->size);

	for(y=0; y<world->size; y++){

		int i = y / minLines;
		Cell *currentLine;
		if(i>=1){
			MPI_Recv(line, world->size*sizeof(Cell)/sizeof(int), MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			currentLine = line;
		}else{
			currentLine = world->mainGrid[y];
		}
		for(x=0; x<world->size; x++){

			if(currentLine[x].type){
				printf("%d %d %c\n",x,y, currentLine[x].type);
			}
		}
	}
}


void world__destroy(World* world)
{
	int i;
	int start = (world->mpi_start>0)?world->mpi_start-1:world->mpi_start;
	int end   = (world->mpi_end<world->size)?world->mpi_end+1:world->mpi_end;
	for(i=start; i<end; i++){

		free(world->mainGrid[i]);
		free(world->tmpGrid[i]);
	}
	free(world->mainGrid);
	free(world->tmpGrid);
	free(world);
}


void world__print(World* world)
{
	return;
	int y,x;
	printf("  |  %2d  | ",0);
	for(y=1; y<world->size; y++){
		printf("%2d  |  ",y);
	}
	printf("\n");

	int start = (world->mpi_start>0)?world->mpi_start-1:world->mpi_start;
	int end   = (world->mpi_end<world->size)?world->mpi_end+1:world->mpi_end;
	for(y=start; y<end; y++){
		printf("%2d|",y);
		for(x=0; x<world->size; x++){

			if(world->mainGrid[y][x].type){
				printf("  %c %d  ", world->mainGrid[y][x].type,world->mainGrid[y][x].breedingPeriod);
			}else{
				printf("   -   ");
			}
		}
		printf("\n");
	}
	printf("\n");
}
