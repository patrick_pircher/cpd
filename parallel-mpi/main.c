#include "wolves-squirrels-mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <time.h>


int main(int argc, char** argv)
{
	int rank, nTotalProcs;
	MPI_Status stat;

	MPI_Init (&argc, &argv);	/* starts MPI */
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
	MPI_Comm_size (MPI_COMM_WORLD, &nTotalProcs);	/* get number of processes */

	WorldParameters params;
	params.iterations = 0;
	params.squirrelsBreedingPeriod = 0;
	params.wolvesBreedingPeriod = 0;
	params.wolvesStarvationPeriod = 0;


	if(argc<6){
		printf("not enough arguments\n");
		return -1;
	}

	if(!(sscanf(argv[2], "%d", &params.wolvesBreedingPeriod)
		 && sscanf(argv[3], "%d", &params.squirrelsBreedingPeriod)
		 && sscanf(argv[4], "%d", &params.wolvesStarvationPeriod)
		 && sscanf(argv[5], "%d", &params.iterations))){
		printf("arguments not valid\n");
		return -1;
	}
	char* filename = argv[1];

	World* world = world__load(params,filename);

	double begin, end;
	double time_spent;

	if(rank==0){
		begin = MPI_Wtime();
	}

	world__run(world);

	MPI_Barrier(MPI_COMM_WORLD);

	if(rank==0){

		end = MPI_Wtime();
		time_spent = (double)(end - begin);
		fprintf(stderr,"time: %f\n", time_spent);

		world__save(world);
	}else{

		int i;
		for(i=world->mpi_start; i<world->mpi_end; i++){
			MPI_Send(world->mainGrid[i], world->size*sizeof(Cell)/sizeof(int), MPI_INT, 0, 0, MPI_COMM_WORLD);
		}
	}
	world__destroy(world);

	MPI_Finalize();
	return 1;
}
