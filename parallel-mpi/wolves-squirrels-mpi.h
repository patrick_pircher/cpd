#ifndef WOLVESSQUIRRELSERIAL_H
#define WOLVESSQUIRRELSERIAL_H

#include <omp.h>

typedef struct WorldParameters{
	int wolvesBreedingPeriod;
	int squirrelsBreedingPeriod;
	int wolvesStarvationPeriod;
	int iterations;
}WorldParameters;

typedef enum Entity{
	Wolf = 'w',
	Squirrel = 's',
	Tree = 't',
	Ice = 'i',
	TreeSquirrel = '$',
	Empty = 0
}Entity;

typedef struct Cell {
	Entity type;
	int breedingPeriod;
	int starvationPeriod;
	int changed;
} Cell, **Grid;

typedef struct World{
	WorldParameters params;
	int size;
	int mpi_start;
	int mpi_end;
	int mpi_rank;
	Grid mainGrid;
	Grid tmpGrid;
}World;

typedef struct Point {
	int x,y;
}Point;


World* world__load(WorldParameters params, char *filename);
void world__run(World* world);
void world__save(World* world);
void world__destroy(World* world);

#endif // WOLVESSQUIRRELSERIAL_H
