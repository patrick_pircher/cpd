import qbs 1.0

Application {

    name: 'parallel-mpi'

    Group {
        name: "Sources"
        files: [
            "*.c"
        ]
        Depends { name:"cpp" }
    }

    Group {
        name: "Headers"
        files: [
            "*.h"
        ]
    }
    Depends { name:"cpp" }
    cpp.cFlags: [
    ]

    cpp.includePaths: ['C:/Program Files\ \(x86\)/OpenMPI_v1.6.2-win32/include']

}


